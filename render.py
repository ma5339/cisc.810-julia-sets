import numpy as np
import matplotlib.pyplot as plt
import configparser
import sys



def complex_power(z, power):
    acumulator=z
    for _ in range(power-1):
        acumulator*=z
    return acumulator


def julia(Re, Im, max_iter, c, norm_limit, power):
    z = complex(Re, Im)    
    for i in range(max_iter):                
        z = complex_power(z, power) + c
        if (z.real*z.real + z.imag*z.imag) >= norm_limit:
            return i
    return max_iter

def render(resolution, bbox, cmap, c_value, max_iter, dpi,norm_limit, power):
    [x1,x2,y1,y2] = bbox
    [rows,columns] = resolution

    result = np.zeros([rows, columns])
    
    for row_index, Re in enumerate(np.linspace(x1, x2, num=rows)):
        for column_index, Im in enumerate(np.linspace(y1, y2, num=columns)):                    
            result[row_index, column_index] = julia(Re, Im, max_iter, c_value, norm_limit, power)

    plt.figure(dpi=dpi)
    plt.imshow(result.T, cmap=cmap, interpolation='bilinear', extent=[x1, x2, y1, y2])            
    plt.show()

def main():
    config = configparser.ConfigParser()
    config.read(sys.argv[1])
    config_values = config['default']
    resolution = np.array(config_values['RESOLUTION'].split(',')).astype(int)
    bbox = np.array(config_values['BBOX'].split(',')).astype(float)
    cmap = config_values['CMAP']
    
    c_values = np.array(config_values['C_VALUE'].split(',')).astype(float)
    c_value = complex(c_values[0],c_values[1])
    
    max_iter = int(config_values['MAX_ITER'])
    dpi = int(config_values['DPI'])
    norm_limit = int(config_values['NORM_LIMIT'])
    power = int(config_values['POWER'])
    render(resolution, bbox, cmap, c_value, max_iter, dpi, norm_limit, power)            
    
    
if __name__ == "__main__":
    main()