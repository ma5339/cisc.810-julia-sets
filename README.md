# CISC.810 Julia Sets
Code provided for visualize Julia Set fractals. Values of $c$ where Julia Sets are non-trivial are provided for the function:


$F(z) = z^n + c$ &emsp; &emsp;  $(1)$

Where $n=2$. These values match the Mandelbrot Set fractal.

## Getting started

The file `render.py` generates a fractal based on the parameters in an initialization file. This `.ini` file has to be provided in the command line. To generate a example Julia Set use:

```
python3 render.py params.ini
```
The file `params.ini` is an example of the parameters needed to generate a fractal. These values are:

- **RESOLUTION**: A *2-tuple* that indicates in how many point will be divided the region of the complex plane where the fractal will be drawed. The bigger the values, the more detailed will be the fractal. Bigger values take more time to compute.
- **BBOX**: A *4-tuple* refering to the region of the complex plane for the fractal to be drawn in. The order of the values are $(r_1,r_2,i_1,i_2)$. Where $r$ and $i$ real and imaginary axis respectively.
- **CMAP**: The color map to be used for drawing the fractal. These values are from the library matplotlib and can be found [here](https://matplotlib.org/stable/tutorials/colors/colormaps.html).
- **C_VALUE**: A *2-tuple* that represent the complex value for $c$ in (1). The first component is the real unit and the second the imaginary unit in a complex number $a+bi$. This value determines the chaotic (non-trivial and visually pleasing) appereance of the fractal. 
- **POWER**: The value for $n$ in (1). Must be a positve integer.
- **NORM_LIMIT**: Escape radius for $z_n$.
- **MAX_ITER**: The max number of iteration for $z_0$ to reach the escape radius. Must be a positve integer. 
- **DPI**: *Dots Per Inch* of the image to be rendered by matplotlib.
## Visualizing $c$ values for $n=2$ in $(1)$
As previously mentioned, $c$ values for the cuadractic function $F(z)=z^2+c$ where the corresponding fractal is visually pleasing match with the Mandelbrot Set. The file `500x500-unique_vals.csv` contains the unique scape values for $250,000$ points in the complex plane. You can visualize this data with this command:
```
python3 visualize_data.py 500x500-unique_vals.csv
```
The brigther points are the $c$ values which corresponding fractal is beautiful.

For a more detailed explanation, refer to [this paper](https://drive.google.com/file/d/1ffFkZCMfPAVUUGTH5rNLgr1QavVJz3ZL/view?usp=sharing).


