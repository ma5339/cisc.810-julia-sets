
import numpy as np
import matplotlib.pyplot as plt
import math
from numpy import genfromtxt
import sys

def main():
    my_data = np.array(genfromtxt(sys.argv[1], delimiter=','))
    a,_ = my_data.shape
    dimension = int(math.sqrt(a))
    result = np.zeros([dimension, dimension])

    x1=-1.5
    x2=1.5
    y1=-1.5
    y2=1.5

    count = 0
    for row_index, _ in enumerate(np.linspace(x1, x2, num=dimension)):
        for column_index, _ in enumerate(np.linspace(y1, y2, num=dimension)):                    
            result[row_index, column_index] = my_data[count][2]
            count+=1


    plt.figure(dpi=200)
    plt.imshow(result.T, cmap='hot', interpolation='bilinear', extent=[-1.5, 1.5, -1.5, 1.5])            
    plt.show()

if __name__ == "__main__":
    main()
